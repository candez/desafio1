import express from 'express';
import * as productos from './productos';
//*Iniciamos una app de express
const app = express();

app.use(express.json());


//*Definimos una ruta y su handler correspondiente
app.get('/welcome', function (request, response){
    response.send('¡Bienvenidos a Express!');
})

//*Creamos los endpoints

app.get('/productos', function(request, response){
    response.send(productos.getStock());
})

app.post('/productos', function(request, response) {
    const body = request.body;
    const id = productos.getStock().length;
    productos.storeProductos(body, id);
    response.send('Agregaremos un producto a la lista')

    console.log(body)
})

app.delete('/productos/:id' , function (request, response) {
    const idProducto = Number(request.params.id);
    productos.deleteProductos(idProducto);
    response.send('Eliminaremos un producto de la lista')
    
}) 

//*Escuchamos la app de express
app.listen(3000, function(){
    console.info('Servidor escuchando en http://localhost:3000');
})