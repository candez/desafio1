// funcion con nombre o nombrada
function suma(x: number , y: number):number {
    return x + y
    }
// funcion flecha
const sumaFlecha = (x: number , y: number):number => {
    return x + y
    }
// console.log(suma(1,2) );
// funcion con parametros opcionales
const funcionOpcionalSuma = (x:number, y?:number):number =>{
    if(!y) return x
    return x + y
    }
// console.log(funcionOpcionalSuma(1));
// console.log(funcionOpcionalSuma(1,2));
// Otra opcion es agregarle un valor por defecto
const funcionOpcionalSuma2 = (x:number, y:number = 0):number =>{
    return x + y
    }
//resta
const restaFlecha = (x: number , y: number):number => {
        return x - y
        }
//multiplicación
const OpcionMultiplicacion = (x: number , y: number):number => {
    return x * y
    }


console.log(funcionOpcionalSuma2(1));
console.log(funcionOpcionalSuma2(1,2));
console.log(restaFlecha(10,5));
console.log(OpcionMultiplicacion(2,5));
    